---
startdate:  2018-11-20
starttime: "19h"
linktitle: "TechTue 499"
title: "TechTuesday 499"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.


# New spot visit: Citygate
We are visiting our next spot at Citygate.  
Address: Rue des Goujons 152, 1070 Anderlecht  
Everyone interested, we meet there at 6PM.

# Regular Techtuesday @ molenbeek
A normal techtuesday will happen at our current spot in Molenbeek from 7pm.