---
startdate:  2018-12-04
starttime: "19h"
linktitle: "TechTue 501"
title: "TechTuesday 501"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
