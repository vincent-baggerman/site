---
linktitle: "Byteweek"
title: "Byteweek"
location: "HSBXL"
eventtype: "Conference"
price: "Free"
series: "byteweek"
--- 

8 days before Fosdem conference, HSBXL compiles ByteWeek.
Eight ‘Bitdays’ of hackatons, workshops and talks.
We end the octaweek with the notorious Bytenight!

## Upcoming Byteweek
{{< events when="upcoming" series="byteweek" >}}

## Past Byteweeks
{{< events when="past" series="byteweek" >}}