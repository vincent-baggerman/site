---
startdate:  2019-01-30
starttime: "19h"
linktitle: "Stompboxin’, DIY fuzzboxes for bass and guitar"
title: "Stompboxin’, DIY fuzzboxes for bass and guitar"
location: "HSBXL"
eventtype: "Workshop"
price: "Free"
series: "bitday2019"
--- 

Stompboxin’, DIY fuzzboxes for bass and guitar