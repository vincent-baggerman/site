---
title: "Move to Citygate"
linktitle: "Citygate"
state: running
image: "citygate.png"
---

HSBXL is moving to Citygate!

# Our contract

- Contract for 3 years
- Warranty is 2 months
- Setup fee of 250€ by Entrakt

# Citygate in the media

- https://www.bruzz.be/en/videoreeks/bruzz-24-2017-06-07/video-citygate-must-breathe-new-life-desolate-industrial-zone
- https://bx1.be/news/anderlecht-le-studio-citygate-occupe-temporairement-par-des-artistes-sportifs-et-artisans
- http://canal.brussels/fr/content/studio-citygate-une-des-plus-grandes-occupations-temporaires-en-belgique
- http://screen.brussels/en/film-commission/movieset/citygate


# Public visit
We visit our new spot on [Tuesday 20/11/2018 6PM](/events/techtuesday/499/).

# Questions for Citygate
Write down all questions we have for the citygate PM

- Can we put the address of our VZW there?
- What with snailmail letters?
- What is the 'préavis'? In other words, how many time do we get when they stop the contract?
- Is there already internet available?
- can we have our own telephone / coax line for our own internet?

# // TODO
- Define packing/moving schedule
- Make truck reservation
- Uninstall water pipe at Molenbeek
- Uninstall heaters
- Uninstall TV's & other stuff hanging on the walls
- Make all desks ready to move
- Uninstall electronics lab
- Create cool street signs to put between south trainstation and the space, mention meters left...
- Arrange access / copy keys
- Fix internet
- Setup the kitchen / the bar
- Pickup moving truck
- Return moving truck

