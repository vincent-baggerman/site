---
title: "Get in contact"
linktitle: "Get in contact"
---


- **Address:** rue de manchester / manchesterstraat 21, 1080
    Molenbeek, Brussels, Belgium
- **Pidgeon:** 50.8479° N, 4.3284° E
    ([GoogleMaps](https://www.google.be/maps/place/HSBXL++Hackerspace+Brussels/@50.8478873,4.3262444,17z/data=!4m5!3m4!1s0x47c3c37ae6875a27:0x962d7fc36e046fa7!8m2!3d50.8478873!4d4.3284331?hl=en))([OpenStreetMap](https://www.openstreetmap.org/node/3664434485))
- **Email:** contact@hsbxl.be
- **Telephone:** +32 2 880 40 04 (phone is in space)
- **Facebook:** [HSBXL on
    Facebook](https://www.facebook.com/groups/hsbxl/)
- **Twitter:** [hsbxl](http://twitter.com/hsbxl) and
    [\#hsbxl](https://twitter.com/search?q=%23hsbxl)
- **Mastodon:**
    [@hsbxl@mastodon.social](https://mastodon.social/@hsbxl)
- **Matrix chat:** \#hsbxl on hackerspaces.be
    ([webchat](https://ptt.hackerspace.be/#/room/#hsbxl:hackerspace.be))
- **IRC:** \#hsbxl on irc.freenode.net
    ([webchat](https://ptt.hackerspaces.be/#/room/#hsbxl:hackerspaces.be))
- **Matrix** HSBXL on
    [ptt.hackerspace.be](https://ptt.hackerspaces.be/#/room/#hsbxl:hackerspace.be)
- **Mailing-list:** see the [ mailing-List](mailing-list "wikilink")
    page
- **Bank:** Argenta IBAN: BE69 9794 2977 5578; BIC: ARSP BE 22
- **Bitcoin:** <https://www.bitkassa.nl/pay/hackerspacebrussels>

# How to get there?

**Address:**  
Manchesterstraat 21,  
1080 Molenbeek,  
Brussels, Belgium

## By public transport 🚆

From Brussels south train station:

  - take metro 2 or 6 till Delacroix (metro each 3 minutes, direction
    simonis / roi baudoin / koning boudewijn)
  - from Delacroix, walk \~650m till HSBXL (Rue Manchester 21)
    [Route](https://www.google.nl/maps/dir/M%C3%A9tro+Station+Delacroix,+Birminghamstraat+98,+1070+Anderlecht/Hackerspace+Brussels+HSBXL,+Manchesterstraat+21,+1080+Brussel/@50.8474325,4.3241349,17z/data=!3m1!4b1!4m14!4m13!1m5!1m1!1s0x47c3c408fb67182d:0x9457582c067c55e1!2m2!1d4.3234784!2d50.8458363!1m5!1m1!1s0x47c3c37ae6875a27:0x962d7fc36e046fa7!2m2!1d4.3284331!2d50.8478873!3e2)

## By car 🚘

We have a private parking for about 20 cars.

# How to get in

- Enter via the big gate of number 21, walk inside to the courtyard.
- The space is located at the 4th floor of the white building. It is reachable by elevator.
- During public events, pressing the blue button on the HSBXL box in the elevator will bring you directly to the 4th floor. (Please take care to always properly close the elevator doors.)

{{< youtube vZiomJd5ar8 >}}

# Statutes at Belgisch Staatsblad / Moniteur Belge
- **06 aug 2009**: [http://www.ejustice.just.fgov.be/tsv_pdf/2009/08/18/09118276.pdf PDF]
- **12 aug 2013**: [http://www.ejustice.just.fgov.be/tsv_pdf/2013/08/22/13131033.pdf PDF]
- **10 may 2016**: [http://www.ejustice.just.fgov.be/tsv_pdf/2016/05/20/16069254.pdf PDF]
- **Trade number**: BE0817617057
